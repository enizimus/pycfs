# include common deps : 
-r common.txt

# dev deps : 
# Documentation deps : 
sphinx==7.2.6
myst-parser==2.0.0
sphinx-book-theme==1.1.2

# Testing deps : 
pytest==8.0.2
pytest-cov==4.1.0

# Pipeline deps : 
mypy==1.8.0
flake8==7.0.0