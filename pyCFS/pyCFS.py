import numpy as np


class pyCFS:
    def __init__(self) -> None:
        pass

    def square_num(self, x: int) -> int:
        return x ** 2

    def double_num(self, x: int) -> int:
        return 2 * x

    def root_num(self, x: float) -> float:
        return np.sqrt(x)
