from pyCFS import pyCFS

def test_square_num():
    pycfs = pyCFS()
    assert pycfs.square_num(5) == 25

def test_double_num():
    pycfs = pyCFS()
    assert pycfs.double_num(5) == 10

def test_root_num():
    pycfs = pyCFS()
    assert pycfs.root_num(16.0) == 4.0