# Developer notes

This section is meant for everyone working on developing the library and committing to the official repository. If you have an own fork of the library then this part does not affect you and might not work as explained below due to access restrictions.

## Development guide

For each new feature, bug fix, etc, please :

- make a new branch
- implement your solution
- make a `pull_request` to merge into `main`

To make sure that all of the commits are valid for pull requests the branch commit must pass all tests. 

The tests are divided into testing the code functionality and coding standard. These are run in the `run_tests` stage : 
- `tests` : runs the tests in `tests` dir
- `lint-type-tests` : checks if types are hinted with `mypy` and coding style with `flake8` (allowed to fail)

To make sure that everything is fine before committing make sure to go over the following checklist :

- [ ] run : `pytest` (existing tests must pass, try to cover most of new code)
- [ ] run : `flake8 pyCFS` (there shouldn't be any errors)
- [ ] run : `mypy pyCFS` (there shouldn't be any errors)

Only after making sure that these tests are ok locally commit the changes to your branch. This way you will make sure to catch any errors that might fail the tests.

## Documentation

Please make sure that you update the documentation along with the features that you implement. This will be checked in the code review.

## Publishing new versions

This part is managed automatically over GitLab CI. New versions of the library are published by pushing a tag to the `main` branch. 

:::{note}
This is only possible if you are a maintainer of the repository.
:::

A new version will be published when agreed upon by the maintainers of the library.
