# Installation 

## Environment setup 

This section covers the setup of the environment used for running the library. Please make sure that you follow the instructions in the given order. 

:::{important}
If you are a developer you still need to perform this setup process. When you have successfully completed it then follow the developer setup instructions.
:::

1. Install python version 3.12. It is recommended to install [anaconda](https://www.anaconda.com/download) or [miniconda](https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html). After doing so, open the command prompt and follow the next steps.
   - Create a new environment with : 
   ```bash
    conda create --name pycfs python=3.12
    ```
   - Switch to new environment with : 
    ```bash
    conda activate pycfs
    ```
2. Install *pyCFS* 
   - If you are using conda as suggested just run :
    ```bash
    python -m pip install --upgrade pycfs
    ```
   - If you are using your system python :
    ```bash
    pip install --upgrade pycfs
    ```

## Developer install 

:::{important}
If you haven't, please first complete the [*Environment setup*](#environment-setup) step.
:::

As a developer you will need to install a few dependencies more than what is needed to just use the package. 

1. Install the requirements by running : 
   ```bash
   python -m pip install -r requirements/dev.txt
   ```
2. To install the package make sure you're in the same directory as the **setup.py** file. If you are 
using an anaconda distribution just run : 
```
$ python -m pip install -e .
```
the `-e` flag (editable install) applies the changes you make while developing directly to the package so that you can easily test it while developing in this source directory.

1. If you are using the system python run : 
```
pip3 install -r requirements.txt
pip3 install -e .
```
This will install the required packages and then the *pycfs* package itself.