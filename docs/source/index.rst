.. pyCFS documentation master file, created by
   sphinx-quickstart on Wed Feb 29 12:44:06 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyCFS's documentation!
=================================

.. note::

   This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   dev_notes